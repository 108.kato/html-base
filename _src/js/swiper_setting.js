import Swiper from 'swiper';
var menuslider;
export function slider() {
    menuslider = new Swiper('#js-menu-slider', {
        slidesPerView: 3.5,
        spaceBetween: 4,
        calculateHeight: true,
        breakpoints: {
            920: {
                slidesPerView: 1.2,
                spaceBetween: 10
            },
            1500: {
                slidesPerView: 2.5,
                spaceBetween: 4
            },
            1900: {
                slidesPerView: 3.5,
                spaceBetween: 4
            },
            2200: {
                slidesPerView: 4.5,
                spaceBetween: 4
            },
            2560: {
                slidesPerView: 5.5,
                spaceBetween: 4
            }
        }
    });
    $(document).on("click", "#js-slide-left", function () {
        menuslider.slidePrev();
    });
    $(document).on("click", "#js-slide-right", function () {
        menuslider.slideNext();
    });
}