const loadGoogleMapsApi = require('load-google-maps-api');
loadGoogleMapsApi(
    {
        // key: 'AIzaSyB50nEFRL2o6tDKnmPMtbFMMLb9evbECHg'
        key: 'AIzaSyCxYg4IENdN32WboeogrR4-kDQ2GwghQrI'
    }
).then(function (googleMaps) {
    var latlng = { lat: 35.671434, lng: 139.762792 };
    var mapArea_footer = document.getElementById('g-map');
    var mapArea_access = document.getElementById('js-access-map');

    var mapOptions = {
        center: latlng,
        zoom: 17,
        disableDefaultUI: true
    };

    if (mapArea_footer) {
        var map = new google.maps.Map(mapArea_footer, mapOptions);
    }
    if (mapArea_access) {
        map2 = new google.maps.Map(mapArea_access, mapOptions)
    }

    window.onload = function () {
        // getWindowWidth();
        if (getWindowWidth() >= 768) {
            //マーカーの設定

            if (mapArea_footer) {
                var markerOptions = {
                    map: map,
                    position: latlng,
                    icon: {
                        url: '/assets/img/cmn/pin@2x.png',
                        scaledSize: new google.maps.Size(64, 72),
                    }
                };
                var marker = new google.maps.Marker(markerOptions);
            }
            if (mapArea_access) {
                var markerOptions2 = {
                    map: map2,
                    position: latlng,
                    icon: {
                        url: '/assets/img/cmn/pin@2x.png',
                        scaledSize: new google.maps.Size(64, 72),
                    }
                };
                var marker2 = new google.maps.Marker(markerOptions2);
            }
        } else {
            //マーカーの設定
            if (mapArea_footer) {
                var markerOptions = {
                    map: map,
                    position: latlng,
                    icon: {
                        url: '/assets/img/cmn/pin@2x.png',
                        scaledSize: new google.maps.Size(32, 36),
                    }
                };
                var marker = new google.maps.Marker(markerOptions);
            }
            if (mapArea_access) {
                var markerOptions2 = {
                    map: map2,
                    position: latlng,
                    icon: {
                        url: '/assets/img/cmn/pin@2x.png',
                        scaledSize: new google.maps.Size(32, 36),
                    }
                };
                var marker2 = new google.maps.Marker(markerOptions2);
            }
        }
    }

    if (mapArea_footer) {
        google.maps.event.trigger(map, "resize");

        google.maps.event.addListener(map, 'bounds_changed', function () {
            var bounds = map.getBounds();
            calculateCenter();
            map.setCenter(center);
        });
    }
    if (mapArea_access) {
        google.maps.event.trigger(map2, "resize");

        google.maps.event.addListener(map2, 'bounds_changed', function () {
            var bounds2 = map2.getBounds();
            calculateCenter();
            map2.setCenter(center2);
        });
    }

    var center
        , center2
        ;
    function calculateCenter() {
        if (mapArea_footer) {
            center = map.getCenter();
        }
        if (mapArea_access) {
            center2 = map2.getCenter();
        }
    }
    function getWindowWidth() {
        var w = document.documentElement.clientWidth;
        return w;
    }
}).catch(function (error) {
    // console.error(error)
});