"use strict";
const gulp = require('gulp');
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');

let path = require('path')
    , fs = require('fs')
    , pkg = JSON.parse(fs.readFileSync('./package.json'))
    , assetsPath = path.resolve(pkg.path.assetsDir)
    ;

function javascript() {
    return gulp
        .src("./_src/js/*.js", [
            path.join(assetsPath, "js/**/*.js"),
            path.join("!", assetsPath, "js/**/*.min.js")
        ])
        .pipe(plumber())
        .pipe(gulp.dest(path.join(assetsPath, "js/")))
        .pipe(uglify())
        .pipe(rename({ extname: ".min.js" }))
        .pipe(gulp.dest(path.join(assetsPath, "js")));
}
exports.javascript = javascript;