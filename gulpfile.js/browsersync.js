"use strict";
const browsersync = require("browser-sync").create();

function browserSync(done) {
    browsersync.init({
        // index: "http://localhost:3000/",
        // startPath: 'index.html',
        port: 3000,
        server: {
            baseDir: "./dist/"
        }
    });
    done();
}
function browserSyncReload(done) {
    browsersync.reload();
    done();
}
exports.browserSync = browserSync;
exports.browserSyncReload = browserSyncReload;