"use strict";
// const { series, parallel } = require("gulp");
const gulp = require("gulp");
const del = require("del");
const { browserSync, browserSyncReload } = require("./browsersync");
const { css } = require("./sass");
const { html } = require("./ejs");
const { images } = require("./copy");
const { webpackScript } = require("./webpack_script");
const { javascript } = require("./script");


// Clean assets
function clean() {
    return del(["./dist/"]);
}

function watchFiles() {
    gulp.watch("_src/scss/**/*.scss", css).on('change', gulp.series(css, browserSyncReload));
    gulp.watch("_src/ejs/**/*.ejs", html).on('change', gulp.series(html, browserSyncReload));
    gulp.watch("_src/ejs/assets/img/**/*", images).on('change', gulp.series(images, browserSyncReload));
    gulp.watch("_src/js/**/*.js", webpackScript).on('change', gulp.series(webpackScript, browserSyncReload));
    // gulp.watch('_src/js/**/*.js', javascript);
}

const build = gulp.series(
    clean,
    gulp.parallel(
        css,
        images,
        html,
        webpackScript
    )
);
const watch = gulp.parallel(
    watchFiles,
    browserSync
);

exports.default = gulp.series(
    build,
    watch
);