"use strict";
const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require("cssnano");
const autoprefixer = require('autoprefixer');
const plumber = require('gulp-plumber');
const rename = require("gulp-rename");
const postcss = require("gulp-postcss");

const path = require('path');
const fs = require('fs');
const pkg = JSON.parse(fs.readFileSync('./package.json'));
const assetsPath = path.resolve(pkg.path.assetsDir);

function css() {
    return gulp
        .src("./_src/scss/**/*.scss")
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest("./dist/assets/css/"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest("./dist/assets/css/"));
}
exports.css = css;